﻿using System.Collections;
using UnityEngine;

public class Pot : MonoBehaviour
{
    [SerializeField] private Animator PotAnimator;

    
    public void SmashPot(){
        PotAnimator.SetBool("Destroy",true);
        StartCoroutine(SetActivePot());

    }
    
    IEnumerator SetActivePot(){
        yield return new WaitForSeconds(0.3f);
        this.gameObject.SetActive(false);
        
    }
    void Update(){

    }
}
