﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class EnemyAI : MonoBehaviour
{
    [SerializeField] private Transform target;

    [SerializeField] private float speed;
    [SerializeField] private float nextWaypointDistance = 3f;

    private Path _path;
    private int _currentWaypoint = 0;
    private bool _reacedEndOfPath = false;

    [SerializeField] private Seeker _seeker;
    [SerializeField] private Rigidbody2D _enemyRigidbody;

    void Start()
    {
        InvokeRepeating("UpdatePath", 0f, .5f);
    }
    void UpdatePath()
    {
        if (_seeker.IsDone())
            _seeker.StartPath(_enemyRigidbody.position, target.position, CompletePath);
    }
    void CompletePath(Path temp_path)
    {
        if (!temp_path.error)
        {
            _path = temp_path;
            _currentWaypoint = 0;
        }
    }
    void FixedUpdate()
    {
        if (_path == null) return;

        if (_currentWaypoint >= _path.vectorPath.Count)
        {
            _reacedEndOfPath = true;
            return;
        }
        else _reacedEndOfPath = false;

        Vector2 direction = ((Vector2)_path.vectorPath[_currentWaypoint] - _enemyRigidbody.position).normalized;
        Vector2 Force = direction * speed * Time.deltaTime;

        _enemyRigidbody.AddForce(Force);

        float distance = Vector2.Distance(_enemyRigidbody.position, _path.vectorPath[_currentWaypoint]);

        if (distance < nextWaypointDistance) _currentWaypoint++;
    }
}
