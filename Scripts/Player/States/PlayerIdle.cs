﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace aro_game
{
    public class PlayerIdle : CharacterState
    {
        public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
        {
            
        }
        public override void OnStateUpdate(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
        {
            if(VirtualInputManager.Instance.MoveBack || VirtualInputManager.Instance.MoveFront || VirtualInputManager.Instance.MoveRight || VirtualInputManager.Instance.MoveLeft)
            {
                animator.SetBool("Move", true);
            }
            if (VirtualInputManager.Instance.Attack)
            {
                animator.SetBool("Attack", true);
            }
        }
        public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
        {
            animator.SetBool("Idle", false);
        }
    }

}