﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace aro_game
{
    public class PlayerMoving : CharacterState
    {
        private float MoveDirection_x = 0f;
        private float MoveDirection_y = 0f;
        public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
        {

        }
        public override void OnStateUpdate(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
        {
            if (!VirtualInputManager.Instance.MoveFront && !VirtualInputManager.Instance.MoveBack && !VirtualInputManager.Instance.MoveLeft && !VirtualInputManager.Instance.MoveRight)
            {
                animator.SetBool("Idle", true);
                return;
            }
            if (VirtualInputManager.Instance.Attack)
            {
                animator.SetBool("Attack",true);
                return;
            }
            FindMoveDirection();

            animator.SetFloat("MoveX", MoveDirection_x);
            animator.SetFloat("MoveY", MoveDirection_y);


        }
        public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
        {
            animator.SetBool("Move", false);
        }
        //Structure for return 2 float variables
        private struct MoveDirection
        {
            public float moveX;
            public float moveY;
        }

        private void FindMoveDirection()
        {
            GameObject PlayerMoveDirection = GameObject.Find("Player");
            PlayerMovement moveDir = PlayerMoveDirection.GetComponent<PlayerMovement>();
            MoveDirection two_directions = new MoveDirection();
            two_directions.moveX = moveDir.MoveDir_x;
            two_directions.moveY = moveDir.MoveDir_y;
            MoveDirection_x = two_directions.moveX;
            MoveDirection_y = two_directions.moveY;
        }
    }

}
