﻿using UnityEngine;

namespace aro_game
{
    public class PlayerAttacking : CharacterState
    {
        private Vector2 _attackangle = Vector2.zero;
        private PlayerState _playerState = PlayerState.idle;

        public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
        {
            _attackangle = FindAttackAngle();
            CheckPlayerState(_playerState);
            if (_playerState != PlayerState.attack)
            {
                // Front attack
                if (_attackangle.y > 0.01f && _attackangle.x > -0.5f && _attackangle.x < 0.5f)
                {
                    animator.SetFloat("AttackX", 0f);
                    animator.SetFloat("AttackY", 1f);
                }
                //Back attack
                if (_attackangle.y < -0.01f && _attackangle.x > -0.5f && _attackangle.x < 0.5f)
                {
                    animator.SetFloat("AttackX", 0f);
                    animator.SetFloat("AttackY", -1f);
                }
                //Left attack
                if (_attackangle.x < -0.01f && _attackangle.y > -0.5f && _attackangle.y < 0.5f)
                {
                    animator.SetFloat("AttackX", -1f);
                    animator.SetFloat("AttackY", 0f);
                }
                //Right attack
                if (_attackangle.x > 0.01f && _attackangle.y < 0.5f && _attackangle.y > -0.5f)
                {
                    animator.SetFloat("AttackX", 1f);
                    animator.SetFloat("AttackY", 0f);
                }
            }
        }
        public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (VirtualInputManager.Instance.MoveBack || VirtualInputManager.Instance.MoveFront || VirtualInputManager.Instance.MoveRight || VirtualInputManager.Instance.MoveLeft)
            {
                animator.SetBool("Move", true);
            }
            else
            {
                animator.SetBool("Idle", true);
            }
        }
        public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
        {
            animator.SetBool("Attack", false);
        }
    }

}


