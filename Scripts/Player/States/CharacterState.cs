﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace aro_game
{
    public class CharacterState : StateMachineBehaviour
    {
        private Rigidbody _Player_Rigidbody;

        protected Rigidbody Find_Rigidbody(Animator animator)
        {
            if (_Player_Rigidbody == null)
            {
                _Player_Rigidbody = animator.GetComponentInParent<Rigidbody>();
            }
            return _Player_Rigidbody;
        }
        protected PlayerState CheckPlayerState(PlayerState temp_state)
        {
            GameObject Object = GameObject.Find("Player");
            PlayerMovement playerState = Object.GetComponent<PlayerMovement>();
            temp_state = playerState.CurrentPlayerState;
            return temp_state;
        }
        protected Vector2 FindAttackAngle()
        {
            GameObject Object = GameObject.Find("Player");
            PlayerAttack attackangle = Object.GetComponent<PlayerAttack>();
            return attackangle.offset;
        }
        private VirtualInputManager characterControl;
        protected VirtualInputManager GetCharacterControl(Animator animator)
        {
            if (characterControl == null)
            {
                characterControl = animator.GetComponentInParent<VirtualInputManager>();
            }
            return characterControl;
        }
    }
}
