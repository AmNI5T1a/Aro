﻿using UnityEngine;

namespace aro_game
{
    public enum PlayerState {
        walk,
        attack,
        interact,
        idle,
    }
    public class PlayerMovement : MonoBehaviour
    {
        [Header("Player Stats")]
        [SerializeField]
        public float Player_Speed;

        [Header("Get Components")]
        [SerializeField]
        private Rigidbody2D Player_Rigidbody = null;
        [SerializeField]
        private Camera MainCamera;

        public PlayerState CurrentPlayerState;

        protected Vector3 Player_Position = Vector2.zero;
        private Vector3 MoveDirection = Vector3.zero;

        public float MoveDir_x;
        public float MoveDir_y;

        private void Start()
        {
            MainCamera = Camera.main;
        }
        void Update() {
            Player_Position.x = Input.GetAxisRaw("Horizontal");
            Player_Position.y = Input.GetAxisRaw("Vertical");

            PlayerMove();
        }

        private void FixedUpdate() {
            Player_Rigidbody.MovePosition(transform.position + MoveDirection * Player_Speed * Time.fixedDeltaTime);
        }

        void PlayerMove()
        {
            MoveDir_x = 0f;
            MoveDir_y = 0f;
            if (VirtualInputManager.Instance.MoveFront)
            {
                MoveDir_y = +1f;
            }
            if (VirtualInputManager.Instance.MoveBack)
            {
                MoveDir_y = -1f;
            }
            if (VirtualInputManager.Instance.MoveLeft)
            {
                MoveDir_x = -1f;
            }
            if (VirtualInputManager.Instance.MoveRight)
            {
                MoveDir_x = +1f;
            }

            MoveDirection = new Vector3(MoveDir_x, MoveDir_y);
        }
    }
}
