﻿using UnityEngine;
using Unity.Collections;
using System.Collections;

namespace aro_game
{
    public class PlayerAttack : MonoBehaviour
    {
        [Header("Components")]
        [SerializeField] private Rigidbody2D _Player_Rigidbody;
        [SerializeField] private Camera _Main_Camera;

        public Transform target;
        public Vector2 offset = Vector2.zero;
        private PlayerState CurrentPlayerState;
        void Start()
        {

        }

        void Update()
        {
            Vector3 Mouse = Input.mousePosition;
            Vector3 ScreenPoint = _Main_Camera.WorldToScreenPoint(target.localPosition);

            // Offset  - help you realize 4 ways attack system
            offset = new Vector2(Mouse.x - ScreenPoint.x, Mouse.y - ScreenPoint.y).normalized;
            FindPlayerState(CurrentPlayerState);

            if(VirtualInputManager.Instance.Attack && CurrentPlayerState.ToString() != aro_game.PlayerState.attack.ToString())
            {
                StartCoroutine(AttackCo());
            }
        }
        public PlayerState FindPlayerState(PlayerState temp_state)
        {
            GameObject Object = GameObject.Find("Player");
            PlayerMovement PlayerState = Object.GetComponent<PlayerMovement>();

            temp_state = PlayerState.CurrentPlayerState;

            return temp_state;
        }
        private IEnumerator AttackCo()
        {
            CurrentPlayerState = PlayerState.attack;
            yield return new WaitForSeconds(0.35f);
            CurrentPlayerState = PlayerState.idle;
        }
    }
}

