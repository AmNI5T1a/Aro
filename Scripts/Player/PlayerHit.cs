﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHit : MonoBehaviour
{
    void Start(){

    }
    void Update(){

    }
    void OnTriggerEnter2D(Collider2D other){
        if(other.CompareTag("BreakableObjects")){
            other.GetComponent<Pot>().SmashPot();
        }
    }
}
