﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace aro_game
{
    public class RoomTransition : MonoBehaviour
    {
        [SerializeField]
        private Vector2 _Change_Min_Camera_Pos = Vector2.zero;
        [SerializeField]
        private Vector2 _Change_Max_Camera_Pos = Vector2.zero;
        [SerializeField]
        private Vector3 _Change_Player_Pos = Vector3.zero;
        [SerializeField]
        private CameraMovement cam = null;
        void Start()
        {
            //cam = Camera.main.GetComponent<CameraMovement>();

        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {
                cam.Min_Camera_Range = _Change_Min_Camera_Pos;
                cam.Max_Camera_Range = _Change_Max_Camera_Pos;

                collision.transform.position += _Change_Player_Pos;
            }
        }
    }
}
