﻿using System.Collections;
using UnityEngine;

public class DeployObjects : MonoBehaviour
{
    [Header("Components")]
    [SerializeField] private GameObject _potPrefab;

    [Header("Variables")]
    [SerializeField] public float RespawnTime = 4f;
    private int ObjectOnScreen = 0;

    void Start()
    {
        StartCoroutine(PotsSpawner());
    }
    private void SpawnPots()
    {
        GameObject a = Instantiate(_potPrefab) as GameObject;
        a.transform.position = new Vector2(Random.Range(-12.83f, -3.85f),
                                            Random.Range(-4.23f, -9.22f));
        ObjectOnScreen += 1;
    }
    IEnumerator PotsSpawner()
    {
        // Future reference for GameIsRunning
        while (true)
        {
            yield return new WaitForSeconds(RespawnTime);
            if (ObjectOnScreen < 4)
            {
                SpawnPots();
            }
        }
    }
}
