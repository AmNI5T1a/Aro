﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace aro_game
{
    public class DialogBoxSign : MonoBehaviour
    {
        [Header("Components")]
        [SerializeField] private GameObject _DialogBox = null;
        [SerializeField] private Text _DialogText = null;

        [Header("Header")]
        [SerializeField] private string _Dialog = null;

        private bool _DialogInRange = false;

        private void Update()
        {
            if (VirtualInputManager.Instance.Use && _DialogInRange)
            {
                _DialogBox.SetActive(true);
                _DialogText.text = _Dialog;

            }
            if (VirtualInputManager.Instance.Close && _DialogInRange || !_DialogInRange)
            {
                _DialogBox.SetActive(false);
            }
        }


        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {
                _DialogInRange = true;
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            _DialogInRange = false;
        }
    }

}