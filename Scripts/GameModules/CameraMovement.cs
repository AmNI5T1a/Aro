﻿using UnityEngine;

namespace aro_game
{
    public class CameraMovement : MonoBehaviour
    {
        [Header("Components")]
        [SerializeField] private Transform _Target = null;

        [Header("Variables")]
        [SerializeField] public float Smoothing;
        [SerializeField] public Vector2 Min_Camera_Range;
        [SerializeField] public Vector2 Max_Camera_Range;

        private void LateUpdate()
        {
            if(transform.position != _Target.position)
            {
                Vector3 Target_Position = new Vector3(_Target.position.x, 
                                                      _Target.position.y,
                                                      transform.position.z);

                Target_Position.x = Mathf.Clamp(Target_Position.x,
                                                Min_Camera_Range.x,
                                                Max_Camera_Range.x);
                Target_Position.y = Mathf.Clamp(Target_Position.y,
                                                Min_Camera_Range.y,
                                                Max_Camera_Range.y);

                transform.position = Vector3.Lerp(transform.position,
                                                  Target_Position,
                                                  Smoothing);


            }
        }
    }

}
