﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace aro_game
{
    public class KeyboardInput : MonoBehaviour
    {
        private void Update()
        {
            if (Input.GetKey(KeyCode.W))
            {
                VirtualInputManager.Instance.MoveFront = true;
            }
            else
            {
                VirtualInputManager.Instance.MoveFront = false;
            }

            if (Input.GetKey(KeyCode.S))
            {
                VirtualInputManager.Instance.MoveBack = true;
            }
            else
            {
                VirtualInputManager.Instance.MoveBack = false;
            }

            if (Input.GetKey(KeyCode.A))
            {
                VirtualInputManager.Instance.MoveLeft = true;
            }
            else
            {
                VirtualInputManager.Instance.MoveLeft = false;
            }

            if (Input.GetKey(KeyCode.D))
            {
                VirtualInputManager.Instance.MoveRight = true;
            }
            else
            {
                VirtualInputManager.Instance.MoveRight = false;
            }

            if (Input.GetKey(KeyCode.Space))
            {
                VirtualInputManager.Instance.Jump = true;
            }
            else
            {
                VirtualInputManager.Instance.Jump = false;
            }

            if (Input.GetKeyDown(KeyCode.E))
            {
                VirtualInputManager.Instance.Use = true;
            }
            else
            {
                VirtualInputManager.Instance.Use = false;
            }
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                VirtualInputManager.Instance.Close = true;
            }
            else
            {
                VirtualInputManager.Instance.Close = false;
            }
            if (Input.GetMouseButtonDown(0))
            {
                VirtualInputManager.Instance.Attack = true;
            }
            else
            {
                VirtualInputManager.Instance.Attack = false;
            } 
        }
    }
}