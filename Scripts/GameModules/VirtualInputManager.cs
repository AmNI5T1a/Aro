﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace aro_game
{
    public class VirtualInputManager : Singleton<VirtualInputManager>
    {
        public bool MoveFront;
        public bool MoveBack;
        public bool MoveRight;
        public bool MoveLeft;
        public bool Jump;
        public bool Use;
        public bool Close;
        public bool Attack;
    }

}

